import QtQuick 2.0
import QtGraphicalEffects 1.0
import QtQuick.Controls 2.2

import Heroes 1.0

Item {
    id: _main
    anchors.fill: parent

    HeroesList {
        id: _heroList

        anchors.top: _main.top
        anchors.topMargin: 100
        anchors.leftMargin: 45
        model: HeroFacade.goodHeroes
    }

    ButtonsList {
        id: _buttonsList

        anchors.centerIn: parent
    }

    HeroesList {
        id: _evilHeroList

        anchors.leftMargin: 20
        anchors.bottom: _main.bottom
        delegateHeight: 110
        delegateWidth: 110
        model: HeroFacade.evilHeroes
    }

    PopupEnhanced {
        id: _popup
    }

    Component.onCompleted: HeroFacade.createHeroes()
}
