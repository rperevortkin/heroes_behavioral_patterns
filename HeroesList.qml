import QtQuick 2.0

ListView {
    id: _heroList

    property int delegateHeight: 150
    property int delegateWidth: 150

    anchors.left: parent.left
    anchors.right: parent.right

    height: 200
    interactive: false
    orientation: ListView.Horizontal

    delegate: Item {
        width: delegateWidth
        height: delegateHeight
        Image {
            anchors.horizontalCenter: parent.horizontalCenter
            width: 100
            height: 100
            source: model.modelData.icon
        }
    }
}
