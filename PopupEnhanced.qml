import QtQuick 2.0
import QtQuick.Controls 2.2

Popup {
    id: _popup

    function openWithText(text) {
        _textEntry.text = text || "Enemy is down";
        open();
    }

    leftMargin: 200
    topMargin: 200
    width: 300
    height: 300

    Rectangle {
        anchors.fill: parent
        color: "lightgreen"
        Text {
            id: _textEntry
            anchors.centerIn: parent
            font.pixelSize: 15
        }
    }
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside
}
