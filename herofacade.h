#ifndef HEROFACADE_H
#define HEROFACADE_H

#include <QObject>
#include <QQmlEngine>

#include "hero.h"
#include "herotype.h"
#include "battlefield.h"

class HeroFacade : public QObject
{

    Q_OBJECT
    Q_PROPERTY(QList<QObject *> goodHeroes READ goodHeroes NOTIFY goodHeroesChanged)
    Q_PROPERTY(QList<QObject *> evilHeroes READ evilHeroes NOTIFY evilHeroesChanged)

public:
    HeroFacade(QObject *parent = nullptr);

    Q_INVOKABLE void createHeroes();
    Q_INVOKABLE void startBattle();
    Q_INVOKABLE void startOneFight();

    static QObject *instance(QQmlEngine *engine, QJSEngine *scriptEngine);

    QList<QObject *> createGoodHeroes();
    QList<QObject *> createEvilHeroes();
    QList<QObject *> goodHeroes() const;
    QList<QObject *> evilHeroes() const;
    bool isGoodHero(HeroType::type heroType);

signals:
    void goodHeroesChanged();
    void evilHeroesChanged();

private:
    BattleField * m_battleField = new BattleField();
    QList<QObject *> m_goodHeroes;
    QList<QObject *> m_evilHeroes;
};

#endif // HEROFACADE_H
