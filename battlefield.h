#ifndef BATTLEFIELD_H
#define BATTLEFIELD_H

#include <QObject>
#include "fightcommand.h"

class BattleField: public QObject
{
    Q_OBJECT
public:
    BattleField(QObject *parent = nullptr): QObject(parent) {}

    void addCommand(FightCommand * command) { m_commands.append(command); }
    void startBattle()
    {
        foreach (auto command, m_commands) {
            command->fight();
        }
        m_commands.clear();
    }

private:
    QList <FightCommand *> m_commands;
};

#endif // BATTLEFIELD_H
