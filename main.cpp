#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "hero.h"
#include "herofacade.h"
#include "herotype.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    qmlRegisterType<Hero>("Heroes", 1, 0, "Hero");
    qmlRegisterUncreatableType<HeroType>("Heroes", 1, 0, "HeroType", "can not instantiate HeroType in qml");
    qmlRegisterSingletonType<HeroFacade>("Heroes", 1, 0, "HeroFacade", &HeroFacade::instance);

    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    return app.exec();
}
