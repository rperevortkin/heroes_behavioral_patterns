#ifndef HEROTYPE_H
#define HEROTYPE_H

#include <QObject>

class HeroType: public QObject
{

    Q_OBJECT
public:
    enum type { WIZARD, WARRIOR, ASSASSIN, NECROMANCER, GOBLIN, ORK, ELF, GOLEM, ROBBER, TROLL };
    Q_ENUM(type)
};

#endif // HEROTYPE_H
