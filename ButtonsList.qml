import QtQuick 2.0

import Heroes 1.0

ListView {

    height: 200
    width: 400

    spacing: 40
    interactive: false
    orientation: ListView.Horizontal

    model: 3

    delegate: Rectangle {
        width: 100
        height: 100
        color: "red"
        radius: 20
        Text {
            anchors.centerIn: parent
            font.pixelSize: 15
            text: index === 0 ? "Start battle" :
                                index === 1 ? "Start one Fight" : "Prepare heroes"
        }
        MouseArea {
            anchors.fill: parent
            onPressed: {
                switch (index)
                {
                case 0:
                    if (HeroFacade.evilHeroes.length > 0) {
                        HeroFacade.startBattle();
                        _popup.openWithText("Battle is ended.\nNo enemies are left");
                    } else {
                        _popup.openWithText("No enemies are left");
                    }
                    break;
                case 1:
                    if (HeroFacade.evilHeroes.length > 0) {
                        HeroFacade.startOneFight();
                        _popup.openWithText();
                    } else {
                        _popup.openWithText("No enemies are left");
                    }
                    break;
                case 2:
                    HeroFacade.createHeroes()
                    _popup.openWithText('Heroes are created');
                    break;
                default:
                    console.warn("Unhandled button action");
                }
            }
        }
    }
}
