#include <QDebug>
#include "herofacade.h"
#include "fightcommand.h"

HeroFacade::HeroFacade(QObject *parent) : QObject(parent) {}

QList<QObject *> HeroFacade::createGoodHeroes()
{
    Hero *necromancer = new Necromancer(QUrl("qrc:/assets/necromancer.jpg"), HeroType::NECROMANCER);
    Hero *assassin = new Assassin(QUrl("qrc:/assets/assassin.jpg"), HeroType::ASSASSIN, necromancer);
    Hero *wizard = new Wizard(QUrl("qrc:/assets/wizard.jpg"), HeroType::WIZARD, assassin);
    Hero *warrior = new Warrior(QUrl("qrc:/assets/warrior.jpg"), HeroType::WARRIOR, wizard);
    return { warrior, wizard, assassin, necromancer };
}

QList<QObject *> HeroFacade::createEvilHeroes()
{
    Hero *goblin = new EvilHero(QUrl("qrc:/assets/goblin.jpeg"), HeroType::GOBLIN);
    Hero *golem = new EvilHero(QUrl("qrc:/assets/golem.jpeg"), HeroType::GOLEM);
    Hero *ork = new EvilHero(QUrl("qrc:/assets/ork.jpeg"), HeroType::ORK);
    Hero *troll = new EvilHero(QUrl("qrc:/assets/troll.jpg"), HeroType::TROLL);
    Hero *robber = new EvilHero(QUrl("qrc:/assets/robber.jpg"), HeroType::ROBBER);
    Hero *elf = new EvilHero(QUrl("qrc:/assets/elf.jpg"), HeroType::ELF);

    return { goblin, golem, ork, troll, robber, elf };
}

void HeroFacade::createHeroes()
{
    m_goodHeroes = createGoodHeroes();
    m_evilHeroes = createEvilHeroes();
    emit goodHeroesChanged();
    emit evilHeroesChanged();
}

bool HeroFacade::isGoodHero(HeroType::type heroType)
{
    QList<HeroType::type> goodHeroes = { HeroType::NECROMANCER, HeroType::ASSASSIN,
                                         HeroType::WARRIOR, HeroType::WIZARD };
    return goodHeroes.contains(heroType);
}

void HeroFacade::startBattle()
{
    QListIterator<QObject *> i(m_evilHeroes);
    while (i.hasNext()) {
        m_battleField->addCommand(new FightCommand(m_goodHeroes.first(), i.next()));
    }
    m_battleField->startBattle();
    m_evilHeroes.clear();
    emit evilHeroesChanged();
}

void HeroFacade::startOneFight()
{
    if (!m_evilHeroes.isEmpty()) {
        m_battleField->addCommand(new FightCommand(m_goodHeroes.first(), m_evilHeroes.first()));
        m_battleField->startBattle();
        m_evilHeroes.removeFirst();
        emit evilHeroesChanged();
    }
}

QObject *HeroFacade::instance(QQmlEngine *engine, QJSEngine *scriptEngine) {

    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)
    static HeroFacade *heroFacade = new HeroFacade();
    return heroFacade;
}

QList<QObject *> HeroFacade::goodHeroes() const
{
    return m_goodHeroes;
}

QList<QObject *> HeroFacade::evilHeroes() const
{
    return m_evilHeroes;
}
