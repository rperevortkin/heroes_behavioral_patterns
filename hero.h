#ifndef HERO_H
#define HERO_H

#include <QObject>
#include <QUrl>
#include <QDebug>
#include "herotype.h"

class Hero : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl icon READ icon CONSTANT)
    Q_PROPERTY(HeroType::type type READ type CONSTANT)
public:
    Hero(QObject *parent = nullptr) : QObject(parent) {}
    Hero(QUrl icon,
         HeroType::type type,
         Hero *hero = nullptr,
         QObject *parent = nullptr): QObject(parent)
    {   m_icon = icon;
        m_type = type;
        m_nextHero = hero; }

    virtual QUrl icon() const { return m_icon; }
    virtual HeroType::type type() const { return m_type; }

    Q_INVOKABLE virtual void fightAgainst(Hero *evilHero)
    {
        if (m_nextHero) {
            m_nextHero->fightAgainst(evilHero);
        } else {
            qWarning() <<"No hero to defeat the enemy";
        }
    }

protected:
    QUrl m_icon;
    HeroType::type m_type;
    Hero *m_nextHero = nullptr;
};


class EvilHero: public Hero
{
public:
    EvilHero(QObject *parent = nullptr) : Hero(parent) {}
    EvilHero(QUrl icon,
             HeroType::type type,
             QObject *parent = nullptr): Hero(icon, type, nullptr, parent) {}
};

class Warrior: public Hero
{

public:
    Warrior(QObject *parent = nullptr) : Hero(parent) {}
    Warrior(QUrl icon,
         HeroType::type type,
         Hero *hero = nullptr,
         QObject *parent = nullptr): Hero(icon, type, hero, parent)
    {
        m_canBeat = { HeroType::GOBLIN, HeroType::ORK, HeroType::ELF };
    }

    void fightAgainst(Hero *evilHero)
    {
        if (m_canBeat.contains(evilHero->type())) {
            qCritical() << "Warrior beats: " << evilHero->type();
            return;
        }
        qWarning() << "Warrior cannot beat this enemy";
        Hero::fightAgainst(evilHero);
    }

private:
    QList<HeroType::type> m_canBeat;
};


class Wizard: public Hero
{

public:
    Wizard(QObject *parent = nullptr) : Hero(parent) {}
    Wizard(QUrl icon,
         HeroType::type type,
         Hero *hero = nullptr,
         QObject *parent = nullptr): Hero(icon, type, hero, parent)
    {
         m_canBeat = { HeroType::GOLEM };
    }

    void fightAgainst(Hero *evilHero)
    {
        if (m_canBeat.contains(evilHero->type())) {
            qCritical() << "Wizard beats: " << evilHero->type();
            return;
        }
        qWarning() << "Wizard cannot beat this enemy";
        Hero::fightAgainst(evilHero);
    }

private:
    QList<HeroType::type> m_canBeat;
};


class Assassin: public Hero
{

public:
    Assassin(QObject *parent = nullptr) : Hero(parent) {}
    Assassin(QUrl icon,
         HeroType::type type,
         Hero *hero = nullptr,
         QObject *parent = nullptr): Hero(icon, type, hero, parent)
    {
         m_canBeat = { HeroType::ROBBER };
    }

    void fightAgainst(Hero *evilHero)
    {
        if (m_canBeat.contains(evilHero->type())) {
            qCritical() << "Assassin beats: " << evilHero->type();
            return;
        }
        qWarning() << "Assassin cannot beat this enemy";
        Hero::fightAgainst(evilHero);
    }

private:
    QList<HeroType::type> m_canBeat;
};


class Necromancer: public Hero
{

public:
    Necromancer(QObject *parent = nullptr) : Hero(parent) {}
    Necromancer(QUrl icon,
         HeroType::type type,
         Hero *hero = nullptr,
         QObject *parent = nullptr): Hero(icon, type, hero, parent)
    {
         m_canBeat = { HeroType::TROLL };
    }

    void fightAgainst(Hero *evilHero)
    {
        if (m_canBeat.contains(evilHero->type())) {
            qCritical() << "Necromancer beats: " << evilHero->type();
            return;
        }
        qWarning() << "Necromancer cannot beat this enemy";
        Hero::fightAgainst(evilHero);
    }

private:
    QList<HeroType::type> m_canBeat;
};

#endif // HERO_H
