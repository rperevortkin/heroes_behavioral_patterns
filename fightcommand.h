#ifndef FIGHTCOMMAND_H
#define FIGHTCOMMAND_H

#include <QObject>
#include "hero.h"

class FightCommand: public QObject
{
    Q_OBJECT
public:
    FightCommand(QObject *hero, QObject *enemy, QObject *parent = nullptr): QObject(parent)
    {
        m_hero = qobject_cast<Hero *>(hero);
        m_enemy = qobject_cast<Hero *>(enemy);
    }

    void fight() { m_hero->fightAgainst(m_enemy); }

private:
    Hero *m_hero;
    Hero *m_enemy;
};

#endif // FIGHTCOMMAND_H
